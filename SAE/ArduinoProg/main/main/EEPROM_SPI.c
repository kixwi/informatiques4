#include <avr/io.h>
#include <avr/delay.h>
#include "EEPROM_SPI.h"
#include "USART.h"
#include "SPI.h"

#define		connexionCS			(PORTB &= ~(1<<CSPORT))
#define		deconnexionCS		(PORTB |= (1<<CSPORT))

uint8_t EEPROM_SPI_Init(void) {
	SPIInit();
	
	DDRB |= (1<<CSPORT);

	connexionCS;
	(void) SPITransmit(WREN);
	deconnexionCS;
		
	connexionCS;
	(void) SPITransmit(WRSR);	
	(void) SPITransmit(0x00);
	deconnexionCS;
		
	return 1;
}

uint8_t EEPROM_SPI_WriteByte(uint8_t data, uint16_t addr) {
	
	WaitForEEPROM_Ready();
	
	//Activation ecriture EEPROM
	connexionCS;
	SPITransmit(WREN);
	deconnexionCS;
	
	WaitForEEPROM_Ready();
		
	//Envoi octet
	uint8_t addrH, addrL;
	uint8_t returnCounter = 0;
	addrH = addr >> 8;
	addrL = addr;
	
	connexionCS;
	
	SPITransmit(WRITE);
	SPITransmit(addrH);
	SPITransmit(addrL);
	SPITransmit(data);
	
	deconnexionCS;
	
	return 1;
}

uint8_t EEPROM_SPI_ReadByte(uint8_t *data, uint16_t addr) {
	
	connexionCS;
	WaitForEEPROM_Ready(); 
	deconnexionCS;
	
	//Lecture data EEPROM
	uint8_t addrH, addrL;
	uint8_t returnCounter = 0;
	addrH = addr >> 8;
	addrL = addr;
	
	connexionCS;
	
	SPITransmit(READ);
	SPITransmit(addrH);
	SPITransmit(addrL);
	SPIReceive(&data);
	
	deconnexionCS;
	
	return data;
}

uint8_t EEPROM_SPI_WriteBlock(uint8_t data, uint16_t addr, int nByte) {
	
}

uint8_t EEPROM_SPI_ReadBlock(uint8_t *data, uint16_t addr, int nByte) {
	
}

void WaitForEEPROM_Ready(void) {
	uint8_t statusEEPROM = 0x00;
	do {
		connexionCS;
		(void) SPITransmit(RDSR);
		(void) SPIReceive(&statusEEPROM);
		deconnexionCS;
	}
	while((statusEEPROM & (1<<0))!=0);
	statusEEPROM = 0x00;
}