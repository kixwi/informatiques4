/*
 * Author : Killian LOUISE TP2 ESE
 *
 * License : CC BY 4.0
 *
 * Rev : 20231210.5
 *
 */
#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "USART.h"

/* Fonction : void USART_Init( unsigned int baudrate )
 * Description : Initialise les param�tres de communication USART.
 *
 * Param�tre :
 *     unsigned int baudrate : Taux de bauds pour la communication USART.
 *
 * Retour : 
 *     Aucun.
 */
void USART_Init( unsigned int baudrate )
{
	/* Set the baud rate */
	UBRR0H = (unsigned char) (baudrate>>8);
	UBRR0L = (unsigned char) baudrate;
	
	/* RAZ du registre de controle A	*/
	UCSR0A = 0x00 ;

	/* Enable UART receiver and transmitter */
	UCSR0B = ( ( 1 << RXEN0 ) | ( 1 << TXEN0 ) );
	
	// Set frame format: 8 data 2stop
	UCSR0C = (1<<USBS0)|(1<<UCSZ01)|(1<<UCSZ00);
	
	//interruptions
	UCSR0B |= (1<<RXCIE0);
	
	//sei();
	
}

/* Fonction : unsigned char USART_Receive( void )
 * Description : Fonction de r�ception USART.
 *
 * Param�tre :
 *     Aucun.
 *
 * Retour :
 *     unsigned char : Donn�e re�ue via USART.
 */
unsigned char USART_Receive( void )
{
	/* Wait for incomming data */
	while ( !(UCSR0A & (1<<RXC0)) )
	;
	
	/* Return the data */
	return UDR0 ;
}

/* Fonction : void USART_Transmit( unsigned char data )
 * Description : Fonction de transmission USART.
 *
 * Param�tre :
 *     unsigned char data : Donn�e � transmettre via USART.
 *
 * Retour :
 *     Aucun.
 */
void USART_Transmit( unsigned char data )
{
	/* Wait for empty transmit buffer */
	while ( !(UCSR0A & (1<<UDRE0)) )
	;
	/* Start transmittion */
	UDR0 = data;
}

/*	Fonction : int ascii_to_int()
 *			Lit une suite de caracteres sur l'USART et les converti en int.
 *
 * 	Parametre :
 * 			Aucun
 *
 * 	Retour :
 *			int value : valeur convertie.
 */
int ascii_to_int() {
	char buffer[10];
	int i = 0;
	unsigned char test = 1;
	int value;

	while(test) {
		char received_char = USART_Receive();
		if(received_char != 'P') {
				buffer[i] = received_char;
				i++;
			} 
		else {
			//Caractere fin de string
			buffer[i] = '\0'; 
			//Conversion avec atoi()
			value = atoi(buffer);
			//RAZ de i
			i = 0;
			//Sortie de la boucle
			test = 0;
		}
	}
	return value;
}