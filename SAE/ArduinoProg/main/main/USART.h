/*
 * Author : Killian LOUISE TP2 ESE
 *
 * License : CC BY 4.0
 *
 * Rev : 20231210.5
 *
 */


#ifndef USART_H_
#define USART_H_

void USART_Init( unsigned int baudrate );
unsigned char USART_Receive( void );
void USART_Transmit( unsigned char data );
int ascii_to_int();



#endif /* USART_H_ */