/*
 * main.c
 *
 * Created: 23/01/2024 17:02:53
 * Author : User
 */ 

#include <avr/io.h>
#include <avr/pgmspace.h>
#include "EEPROM_SPI.h"
#include "USART.h"


#include <stdio.h>
#include <string.h>

#define DEBUG1


static FILE stdoutUSART = FDEV_SETUP_STREAM(USART_Transmit, NULL, _FDEV_SETUP_WRITE);

void setup(void) {
	USART_Init(51);
	
	stdout = &stdoutUSART;
	
	//Init EEPROM
	EEPROM_SPI_Init();
	

}

int main(void)
{
	setup();
	
	uint8_t c;
	
	unsigned char stringAEcrire[100] = "Bonjour!\n\rHello ";
	static const unsigned char logoIUT1[2500] PROGMEM = {
			"                                                                      \n\r"
			"                                                        @             \n\r"
			"                                                      @@              \n\r"
			"                                                     @@@              \n\r"
			"                                                    @@@               \n\r"
			"                                                   @@@                \n\r"
			"                                                 @@@@                 \n\r"
			"                                                @@@                   \n\r"
			"                                              @@@@                    \n\r"
			"                                            @@@@                      \n\r"
			"                 @@@@                     @@@@                        \n\r"
			"               @    =@@                 @@@@                          \n\r"
			"               @% .  @@               @@@@     @                      \n\r"
			"                @@@@@@              @@@      @@@                      \n\r"
			"                                 @@@@     @@@@@@                      \n\r"
			"                    @@        @@@@     @@   @@@@@@@@@@@@              \n\r"
			"                %@@@@@     @@@@     @@@@@    @@@@                     \n\r"
			"                 @@@@@  @@@          @@@@    @@@                      \n\r"
			"                  @@@@@      @       @@@@    @@@                      \n\r"
			"                @@@@@@   @@@@@       @@@@    @@@                      \n\r"
			"                  @@@@    @@@@       @@@@    @@@                      \n\r"
			"                  @@@@    =@@@       @@@@    @@@                      \n\r"
			"                  @@@@    %@@@       @@@@    @@@                      \n\r"
			"                  @@@@    %@@@       @@@@    @@@                      \n\r"
			"                  @@@@    %@@@       @@@@    @@@                      \n\r"
			"                  @@@@    *@@@       @@@@    @@@@                     \n\r"
			"                  @@@@     @@@@    @@@@@@     @@@@@    @              \n\r"
			"               @@@@@@@@@@@   @@@@@   @@@@@@    @@@@@@@@               \n\r"
			"                                                                      \n\r"};
	

	for(int i = 0; i<=3000; i++) {
		(void) EEPROM_SPI_WriteByte('.', i);
	}
	
	/*
	uint16_t position = 0; 
	for(int j = 0; j<=30; j++) {
		for(int i = 0; i<=80; i++) {
			position = (j*80) + i;
			(void) EEPROM_SPI_WriteByte(pgm_read_byte(&(logoIUT1[i][j])), position);
		}
	}
	*/
	
	
	for(int i = 0; i<=2500; i++) {
		(void) EEPROM_SPI_WriteByte(pgm_read_byte(&(logoIUT1[i])), i);
	}
	
	/*
	for(int i = 0; i<=2000; i++) {
		(void) EEPROM_SPI_WriteByte(stringAEcrire[i], i);
	}
	*/
		
	for(int i = 0; i<=2500; i++) {
		USART_Transmit(EEPROM_SPI_ReadByte(&c, i));
	}
	
    while (1) 
    {
		
    }
}

