/*
 * EEPROM_SPI.h
 *
 * Created: 23/01/2024 17:03:29
 *  Author: User
 */ 


#ifndef EEPROM_SPI_H_
#define EEPROM_SPI_H_

#define		WREN		(0x06)
#define		WRDI		(0x04)
#define		RDSR		(0x05)
#define		WRSR		(0x01)
#define		READ		(0x03)
#define		WRITE		(0x02)

#define		RDY			0

#define		CSPORT		PORTB2

uint8_t EEPROM_SPI_Init(void);
uint8_t EEPROM_SPI_WriteByte(uint8_t data, uint16_t addr);
uint8_t EEPROM_SPI_ReadByte(uint8_t *data, uint16_t addr);
uint8_t EEPROM_SPI_WriteBlock(uint8_t data, uint16_t addr, int nByte);
uint8_t EEPROM_SPI_ReadBlock(uint8_t *data, uint16_t addr, int nByte);

void WaitForEEPROM_Ready(void);

#endif /* EEPROM_SPI_H_ */