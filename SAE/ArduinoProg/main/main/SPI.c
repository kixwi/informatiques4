#include "SPI.h"
#include <avr/io.h>

void SPIInit(void) {
	DDRB |= (1<<DDB3) | (1<<DDB5) | (1<<DDB2);

	SPCR = (1<<SPE) | (1<<MSTR) | (1<<SPR0) | (1<<SPR1) | (1<<CPOL) | (1<<CPHA);
	//SPSR |= (1<<SPI2X);
	
	//Toggle CS pour passage mode actif (4 de la doc)
	PORTB |= (1<<PORTB2);
	
	PORTB &= ~(1<<PORTB2);
	
	PORTB |= (1<<PORTB2);
	
	PORTB &= ~(1<<PORTB2);
	
	PORTB |= (1<<PORTB2);
}

void SPITransmit(uint8_t data) {
	SPDR = data;
	
	while(!(SPSR & (1<<SPIF)));
}

void SPIReceive(uint8_t *data) {
	SPDR = 0xFF;
	while(!(SPSR & (1<<SPIF)));
	
	*data = SPDR;
}