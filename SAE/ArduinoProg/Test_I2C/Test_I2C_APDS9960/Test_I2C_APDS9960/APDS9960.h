/*
 * APDS9960.h
 *
 * Created: 04/03/2024 10:43:15
 *  Author: User
 */ 


#ifndef APDS9960_H_
#define APDS9960_H_

/* Definition des adresses registres 				                         */

#define 	RAM			0x00	/* RAM										 */
#define		ENABLE		0x80	/* Enable states and interrupts				 */
#define		ATIME		0x81	/* ADC integration time						 */
#define		WTIME		0x83	/* Wait time (non-gesture)					 */
#define		AILTL		0x84	/* ALS interrupt low threshold low byte		 */
#define		AILTH		0x85	/* ALS interrupt low threshold high byte	 */
#define		AIHTL		0x86	/* ALS interrupt high threshold low byte	 */
#define		AIHTH		0x87	/* ALS interrupt high threshold high byte	 */
#define		PILT		0x89	/* Proximity interrupt low threshold		 */
#define		PIHT		0x8B	/* Proximity interrupt high threshold		 */
#define		PERS		0x8C	/* Interrupt persistence filters			 */
#define		CONFIG1		0x8D	/* Configuration register one				 */
#define 	PPULSE		0x8E	/* Proximity pulse count and length			 */
#define		CONFIG2		0x90	/* Configuration register two				 */
#define		ID			0x92	/* Device ID 								 */
#define		STATUS		0x93	/* Device status							 */
#define		CDATAL		0x94	/* Low byte of clear channel data			 */
#define		CDATAH		0x95	/* High byte of clear channel data			 */


#endif /* APDS9960_H_ */